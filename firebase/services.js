import {
  collection,
  addDoc,
  doc,
  increment,
  deleteDoc,
  updateDoc,
  setDoc,
  arrayUnion,
  arrayRemove,
  where,
  getDocs,
  query,
  getDoc,
} from "firebase/firestore";
import { ref, uploadString, getDownloadURL } from "firebase/storage";
import { db, storage } from "./clientApp";

const productsRef = collection(db, "products");
const salesRef = collection(db, "sales");
const usersRef = collection(db, "users");

const uploadProductImage = (name, file) => {
  console.log({
    name,
    file,
  });
  return new Promise((resolve, reject) => {
    const storageRef = ref(storage, "products/" + `${name}`);

    fetch(file)
      .then((res) => res.blob())
      .then((blob) => {
        let reader = new FileReader();
        reader.readAsDataURL(blob);
        reader.onloadend = function () {
          let base64data = reader.result;
          console.log(base64data);
          uploadString(storageRef, base64data, "data_url")
            .then((snapshot) => {
              if (snapshot) {
                console.log("Started upload");
                const progress =
                  (snapshot.bytesTransferred / snapshot.totalBytes) * 100;

                switch (snapshot.state) {
                  case "paused":
                    console.log("Upload is paused");
                    break;
                  case "running":
                    console.log("Upload is running");
                    break;
                }

                return snapshot.ref;
              }
            })
            .then((ref) => {
              console.log("Ref generated");
              getDownloadURL(ref)
                .then((downloadURL) => {
                  resolve(downloadURL);
                  console.log("File available at", downloadURL);
                })
                .catch((err) => reject(err));
            });
        };
      });
  });
};

const generateKeywords = (productName) => {
  const wordArr = productName.toLowerCase().split(" ");
  const searchableKeywords = [];
  let prevKey = "";
  for (const word of wordArr) {
    const charArr = word.toLowerCase().split("");
    for (const char of charArr) {
      const keyword = prevKey + char;
      searchableKeywords.push(keyword);
      prevKey = keyword;
    }
    prevKey = "";
  }
  return searchableKeywords;
};

const createProduct = async (args) => {
  const docRef = await addDoc(productsRef, {
    name: args.name,
    price: args.price,
    quantity: args.quantity,
    category: args.category,
    image: args.image,
    offer_price: null,
    on_offer: false,
    added: Date.now(),
    searchableKeywords: generateKeywords(args.name),
  });

  return docRef;
};

const getSales = async () => {
  let sales = await getDocs(query(collection(db, "sales")));
  return sales;
};

const fetchProducts = async (args) => {
  const { _query } = args;
  const q = query(
    collection(db, "products"),
    where("searchableKeywords", "array-contains", _query.trim().toLowerCase())
  );

  let products = (await getDocs(q)).docs;
  return products;
};

const createUser = async (args) => {
  await setDoc(doc(db, "users", args.uid), {
    displayName: args.displayName,
    email: args.email,
    phoneNumber: args.phoneNumber,
    photo: args.photo == null ? null : args.photo,
  });
  await updateDoc(doc(db, "meta", `counts`), { users: increment(1) });
};

const updateUser = async (update, id) => {
  await updateDoc(doc(db, "users", id), update);
};

const addOffer = async (args) => {
  const { offer_price, startDate, endDate } = args;
  let data = {
    offer_price,
    start_date: startDate,
    end_date: endDate,
  };
  await updateDoc(doc(db, "products", `${args.id}`), data);
};

// const updateCount = async (args) => {
//   const { newCount, field } = args;

//   let data = {}

//   switch (field) {
//     case 'products':
//       data.products =
//       break;

//     default:
//       break;
//   }
// }

const updateProduct = async (args) => {
  const { id, name, price, category, quantity } = args;
  let data = {
    name,
    price,
    category,
    quantity,
  };
  await updateDoc(doc(db, "products", `${id}`), data);
};

const deleteProduct = async (args) => {
  await deleteDoc(doc(db, "products", args.id));
  await updateDoc(doc(db, "meta", `counts`), { products: increment(-1) });
};

const addToCart = async (args) => {
  const { product, user } = args;
  let itemMeta = {
    added: Date.now(),
    delivered: false,
    packed: false,
    paid: false,
    product,
    quantity: 1,
  };
  await updateDoc(doc(db, "users", `${user}`), {
    cart: arrayUnion(itemMeta),
  });
};

const createSale = async (args) => {
  const docRef = await addDoc(salesRef, {
    quantity: args.quantity,
    customer: args.customer,
    phone: args.phone,
    product: {
      name: args.product,
      price: args.price,
    },
  });

  console.log(docRef);

  return docRef;
};

const decrementCartProduct = async (args) => {
  let docSnap = await getDoc(doc(db, "users", args.user));

  return new Promise((resolve, reject) => {
    if (docSnap.exists()) {
      let new_cart = [
        ...docSnap
          .data()
          .cart.filter((cartItem) => cartItem.product.id !== args.id),
        {
          delivered: docSnap
            .data()
            .cart.filter((cartItem) => cartItem.product.id == args.id)[0]
            .delivered,
          added: docSnap
            .data()
            .cart.filter((cartItem) => cartItem.product.id == args.id)[0].added,
          packed: docSnap
            .data()
            .cart.filter((cartItem) => cartItem.product.id == args.id)[0]
            .packed,
          paid: docSnap
            .data()
            .cart.filter((cartItem) => cartItem.product.id == args.id)[0].paid,
          product: docSnap
            .data()
            .cart.filter((cartItem) => cartItem.product.id == args.id)[0]
            .product,
          quantity:
            docSnap
              .data()
              .cart.filter((cartItem) => cartItem.product.id == args.id)[0]
              .quantity - 1,
        },
      ];

      console.log(new_cart);

      updateDoc(doc(db, "users", args.user), {
        cart: new_cart,
      })
        .then(() => resolve(true))
        .catch(() => resolve(false));
    } else {
      resolve(false);
    }
  });
};

const incrementCartProduct = async (args) => {
  let docSnap = await getDoc(doc(db, "users", args.user));

  return new Promise((resolve, reject) => {
    if (docSnap.exists()) {
      let new_cart = [
        ...docSnap
          .data()
          .cart.filter((cartItem) => cartItem.product.id !== args.id),
        {
          delivered: docSnap
            .data()
            .cart.filter((cartItem) => cartItem.product.id == args.id)[0]
            .delivered,
          added: docSnap
            .data()
            .cart.filter((cartItem) => cartItem.product.id == args.id)[0].added,
          packed: docSnap
            .data()
            .cart.filter((cartItem) => cartItem.product.id == args.id)[0]
            .packed,
          paid: docSnap
            .data()
            .cart.filter((cartItem) => cartItem.product.id == args.id)[0].paid,
          product: docSnap
            .data()
            .cart.filter((cartItem) => cartItem.product.id == args.id)[0]
            .product,
          quantity:
            docSnap
              .data()
              .cart.filter((cartItem) => cartItem.product.id == args.id)[0]
              .quantity + 1,
        },
      ];

      console.log(new_cart);

      updateDoc(doc(db, "users", args.user), {
        cart: new_cart,
      })
        .then(() => resolve(true))
        .catch(() => resolve(false));
    } else {
      resolve(false);
    }
  });
};

//obsolette
const incrementOrder = async (args) => {
  const docRef = await updateDoc(doc(db, "orders", args.id), {
    quantity: increment(1),
  });
  return docRef;
};

//obsolette
const decrementOrder = async (args) => {
  const docRef = await updateDoc(doc(db, "orders", args.id), {
    quantity: increment(-1),
  });
  return docRef;
};

//obsolette
const deleteOrder = async (args) => {
  const docRef = await deleteDoc(doc(db, "orders", args.id));
  return docRef;
};

const markPacked = async (args) => {
  let docSnap = await getDoc(doc(db, "users", args.user));

  return new Promise((resolve, reject) => {
    if (docSnap.exists()) {
      let newCart = [];

      for (let i = 0; i < docSnap.data().cart.length; i++) {
        let cartItem = {};
        if (
          (docSnap.data().cart[i].paid == true ||
            docSnap.data().cart[i].deliver_first == true) &&
          docSnap.data().cart[i].packed == false &&
          docSnap.data().cart[i].delivered == false
        ) {
          cartItem.product = docSnap.data().cart[i].product;
          cartItem.added = docSnap.data().cart[i].added;
          cartItem.quantity = docSnap.data().cart[i].quantity;
          cartItem.paid = docSnap.data().cart[i].paid;
          cartItem.deliver_first = docSnap.data().cart[i].deliver_first;
          cartItem.packed = true;
          cartItem.delivered = false;
          newCart[i] = cartItem;
        } else {
          cartItem.product = docSnap.data().cart[i].product;
          cartItem.added = docSnap.data().cart[i].added;
          cartItem.quantity = docSnap.data().cart[i].quantity;
          cartItem.paid = docSnap.data().cart[i].paid;
          cartItem.packed = docSnap.data().cart[i].packed;
          cartItem.delivered = docSnap.data().cart[i].delivered;
          cartItem.deliver_first = docSnap.data().cart[i].deliver_first;
          newCart[i] = cartItem;
        }
      }

      console.log(newCart);

      updateDoc(doc(db, "users", args.user), {
        cart: newCart,
      })
        .then(() => resolve(true))
        .catch(() => resolve(false));
    }
  });
};

const markDelivered = async (args) => {
  let docSnap = await getDoc(doc(db, "users", args.user));

  return new Promise((resolve, reject) => {
    if (docSnap.exists()) {
      let newCart = [];

      for (let i = 0; i < docSnap.data().cart.length; i++) {
        let cartItem = {};
        if (
          (docSnap.data().cart[i].paid == true ||
            docSnap.data().cart[i].deliver_first == true) &&
          docSnap.data().cart[i].packed == true &&
          docSnap.data().cart[i].delivered == false
        ) {
          cartItem.product = docSnap.data().cart[i].product;
          cartItem.added = docSnap.data().cart[i].added;
          cartItem.quantity = docSnap.data().cart[i].quantity;
          cartItem.packed = docSnap.data().cart[i].packed;
          cartItem.deliver_first = docSnap.data().cart[i].deliver_first;
          cartItem.delivered = true;
          if (docSnap.data().cart[i].paid == false) {
            cartItem.paid = true;
          } else {
            cartItem.paid = docSnap.data().cart[i].paid;
          }
          newCart[i] = cartItem;

          createSale({
            quantity: docSnap.data().cart[i].quantity,
            customer: docSnap.data().displayName,
            phone: docSnap.data().phoneNumber,
            added: Date.now(),
            product: {
              name: docSnap.data().cart[i].product.name,
              price: docSnap.data().cart[i].product.price,
            },
          }).then((val) => console.log(val));
        } else {
          cartItem.product = docSnap.data().cart[i].product;
          cartItem.added = docSnap.data().cart[i].added;
          cartItem.quantity = docSnap.data().cart[i].quantity;
          cartItem.paid = docSnap.data().cart[i].paid;
          cartItem.packed = docSnap.data().cart[i].packed;
          cartItem.delivered = docSnap.data().cart[i].delivered;
          cartItem.deliver_first = docSnap.data().cart[i].deliver_first;
          newCart[i] = cartItem;
        }
      }

      console.log(newCart);

      updateDoc(doc(db, "users", args.user), {
        cart: newCart,
      })
        .then(() => resolve(true))
        .catch(() => resolve(false));
    }
  });
};

const removeFromCart = async (args) => {
  let docSnap = await getDoc(doc(db, "users", args.user));

  return new Promise((resolve, reject) => {
    if (docSnap.exists()) {
      let new_cart = [
        ...docSnap
          .data()
          .cart.filter((cartItem) => cartItem.product.id !== args.id),
      ];

      console.log(new_cart);

      updateDoc(doc(db, "users", args.user), {
        cart: new_cart,
      })
        .then(() => resolve(true))
        .catch(() => resolve(false));
    } else {
      resolve(false);
    }
  });
};

const verifyOnOffer = (startDate, endDate) => {
  if (
    Date.now() > new Date(startDate).getTime() &&
    Date.now() < new Date(endDate).getTime()
  ) {
    return true;
  } else {
    return false;
  }
};

const DB_Services = {
  createProduct,
  addToCart,

  incrementOrder,
  decrementOrder,
  decrementCartProduct,
  incrementCartProduct,
  removeFromCart,
  markPacked,
  markDelivered,
  deleteOrder,
  updateProduct,
  uploadProductImage,
  deleteProduct,
  addOffer,
  createUser,
  updateUser,
  getSales,
  verifyOnOffer,
  productsRef,
  fetchProducts,
};

export default DB_Services;
