import Container from "../components/Container";
import {
  Input,
  Form,
  Typography,
  InputNumber,
  Select,
  Empty,
  message,
  Row,
  Image,
  Col,
  Tabs,
  Avatar,
  Popconfirm,
  DatePicker,
  Tag,
  Divider,
  Menu,
  Statistic,
  Dropdown,
  Modal,
  Button,
  Card,
  Spin,
  Tooltip,
  Badge,
} from "antd";

import { useState, useEffect, useRef } from "react";

import {
  FilterOutlined,
  MoreOutlined,
  SearchOutlined,
  CodeSandboxOutlined,
  UserOutlined,
  MoneyCollectOutlined,
  DollarOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import DB_Services from "../firebase/services";

import Compressor from "compressorjs";

import { db } from "../firebase/clientApp";
import {
  query,
  collection,
  orderBy,
  limit,
  getDocs,
  startAfter,
  onSnapshot,
  where,
  doc,
} from "firebase/firestore";

import { useCollection, useDocument } from "react-firebase-hooks/firestore";

const { Option } = Select;
const { RangePicker } = DatePicker;
const { TabPane } = Tabs;
const { Title, Text, Paragraph } = Typography;
const { Search } = Input;

export default function Admin() {
  return <Dashboard />;
}

const Dashboard = () => {
  const ProductsComponent = (product) => {
    const [addModal, setAddModal] = useState(false);
    const [productName, setProductName] = useState("");
    const [productPrice, setProductPrice] = useState();
    const [productQuantity, setProductQuantity] = useState();
    const [category, setCategory] = useState("sodas");

    const [image, setImage] = useState();
    const [imageUrl, setImageURL] = useState();
    const [uploadLoading, setLoadingUpload] = useState(false);
    const [keyword, setKeyword] = useState("");

    const [filter, setFilter] = useState("");
    const [loadingMore, setLoadingMore] = useState(false);
    const [loadingSearch, setLoadingSearch] = useState(false);
    const [prods, setProds] = useState([]);
    const [lastKey, setLastKey] = useState("");
    const [mutation, setMutation] = useState(0);
    const [suggestions, setSuggestions] = useState([]);
    const [loadingProduct, setLoadingProduct] = useState("");

    const scrollDiv = useRef();

    const [items, itemsLoading, itemsError] = useCollection(
      query(collection(db, "products"), orderBy("added", "desc"))
    );

    const getProducts = async (doc) => {
      setLoadingMore(true);

      let data;

      if (filter == "") {
        data = query(
          collection(db, "products"),
          orderBy("added", "desc"),
          startAfter(doc),
          limit(15)
        );
      } else {
        data = query(
          collection(db, "products"),
          where("category", "==", filter),
          orderBy("added", "desc"),
          startAfter(doc),
          limit(15)
        );
      }

      const snapShot = await getDocs(data);

      setLastKey(snapShot.docs[snapShot.docs.length - 1]);

      let _data_ = [];

      snapShot.docs.forEach((doc) => {
        _data_[_data_.length] = { id: doc.id, data: doc.data() };
      });

      setProds(prods.concat(_data_));
    };

    const fetchMore = () => {
      if (
        scrollDiv.current.offsetHeight + scrollDiv.current.scrollTop >=
        scrollDiv.current.scrollHeight
      ) {
        getProducts(lastKey).then(() => setLoadingMore(false));
      }
    };

    useEffect(() => {
      if (image) {
        new Compressor(image, {
          quality: 0.6, // 0.6 can also be used, but its not recommended to go below.
          success: (compressedResult) => {
            // compressedResult has the compressed file.
            // Use the compressed file to upload the images to your server.
            const _imgURL = URL.createObjectURL(compressedResult);
            setImageURL(_imgURL);
            console.log(_imgURL);
          },
        });
      }
      return;
    }, [image]);

    useEffect(async () => {
      let data;

      if (filter == "") {
        data = query(
          collection(db, "products"),
          orderBy("added", "desc"),
          limit(15)
        );
      } else {
        data = query(
          collection(db, "products"),
          where("category", "==", filter),
          orderBy("added", "desc"),
          limit(15)
        );
      }

      const snapShot = await getDocs(data);

      let _data_ = [];

      snapShot.docs.forEach((doc) => {
        _data_[_data_.length] = { id: doc.id, data: doc.data() };
      });

      setLastKey(snapShot.docs[snapShot.docs.length - 1]);
      setProds(_data_);
    }, []);

    useEffect(async () => {
      setProds([]);
      let data;

      if (filter == "") {
        data = query(
          collection(db, "products"),
          orderBy("added", "desc"),
          limit(15)
        );
      } else {
        data = query(
          collection(db, "products"),
          where("category", "==", filter),
          orderBy("added", "desc"),
          limit(15)
        );
      }

      const snapShot = await getDocs(data);

      let _data_ = [];

      snapShot.docs.forEach((doc) => {
        _data_[_data_.length] = { id: doc.id, data: doc.data() };
      });

      setLastKey(snapShot.docs[snapShot.docs.length - 1]);
      setProds(_data_);
    }, [filter, imageUrl, mutation]);

    const handleSearch = async (val) => {
      setLoadingProduct(true);

      let _suggestions = await DB_Services.fetchProducts({ _query: val });
      let _data = [];
      _suggestions.forEach((sug) => {
        let data = {
          id: sug.id,
          ...sug.data(),
        };

        _data.push(data);
      });
      setSuggestions([..._data]);
      setLoadingProduct(false);
    };

    const handleOkAdd = () => {
      if (!productName) {
        message.warning("Product name required to save image!");
      } else {
        setLoadingUpload(true);

        DB_Services.uploadProductImage(productName, imageUrl)
          .then((productImageURL) => {
            let _product = {
              name: productName,
              price: productPrice,
              quantity: productQuantity,
              category,
              image: productImageURL,
            };
            console.log(_product);
            DB_Services.createProduct(_product)
              .then(() => {
                message.success(
                  `Successfully added '${productName}' to the database`
                );
              })
              .then(() => setLoadingUpload(false))
              .then(() => {
                setAddModal(false);
                setCategory("sodas");
                setProductQuantity(null);
                setProductName(null);
                setProductPrice(null);
                setImage();
                setImageURL();
              })
              .catch((err) => message.error("Oops! Server error"));
          })
          .catch((err) => message.error("Failed to upload product image"));
      }
    };

    const onImageChange = (e) => {
      setImage(e.target.files[0]);
    };

    const AdminProduct = ({ data, id, _highlighted }) => {
      const [offersModal, setOffersModal] = useState(false);
      const [editModal, setEditModal] = useState(false);
      const [offer_price, setOfferPrice] = useState();
      const [startDate, setStartDate] = useState();
      const [endDate, setEndDate] = useState();
      const [loading, setLoading] = useState(false);
      const [loadingEdit, setLoadingEdit] = useState(false);
      const [editedPrice, setEditedPrice] = useState(data.price);
      const [editedName, setEditedName] = useState(data.name);
      const [editedCategory, setEditedCategory] = useState(data.category);
      const [editedQuantity, setEditedQuantity] = useState(data.quantity);

      const addToOffers = () => {
        setOffersModal(true);
      };

      const confirmDelete = () => {
        DB_Services.deleteProduct({ id })
          .then(() => {
            setMutation(mutation + 1);
            message.success(`'${data.name}' removed`);
          })
          .catch((err) => console.log(err));
      };

      const handleOkOffers = () => {
        if (offer_price == null || startDate == null || endDate == null) {
          message.warning("Please fill in the missing field");
        } else {
          setLoading(true);
          let update = {
            offer_price,
            startDate,
            endDate,
            id,
          };
          DB_Services.addOffer(update)
            .then(() =>
              message.success(`Successfully added '${data.name}' to offers!`)
            )
            .then(() => setLoading(false))
            .then(() => setOffersModal(false))
            .then(() => setMutation(mutation + 1))
            .catch(() => message.error("Oops! Failed. Try again later"));
        }
      };

      const handleOkEdit = () => {
        setLoadingEdit(true);
        let _product = {
          id,
          name: editedName,
          price: editedPrice,
          category: editedCategory,
          quantity: editedQuantity,
        };
        DB_Services.updateProduct(_product)
          .then(() => message.success("Updated!"))
          .then(() => setMutation(mutation + 1))
          .then(() => setLoadingEdit(false))
          .then(() => setEditModal(false));
      };

      const handleCancel = () => {
        setOffersModal(false);
      };

      const menu = (
        <Menu>
          <Menu.Item key="0">
            <a onClick={addToOffers}>Add to offers</a>
          </Menu.Item>
          <Menu.Divider />
          <Menu.Item key="3">
            <a onClick={() => setEditModal(true)}>Edit product</a>
          </Menu.Item>
          <Menu.Divider />
          <Menu.Item key="4">
            <Popconfirm
              title="Are you sure to delete this product?"
              onConfirm={confirmDelete}
              okText="Yes"
              cancelText="No"
            >
              <a>Delete product</a>
            </Popconfirm>
          </Menu.Item>
        </Menu>
      );

      return (
        <section
          style={{
            width: "100%",
            marginBottom: 0,
            zIndex: 99,
            maxWidth: 400,
            margin: "0px 0px 12px 12px",
          }}
        >
          <Row
            style={{
              position: "relative",
              background: _highlighted ? "rgba(229,224,85,0.2)" : "#f1f1f1",
              marginBottom: 8,
              padding: 8,
              height: 116,
            }}
          >
            {DB_Services.verifyOnOffer(data.start_date, data.end_date) && (
              <Tag
                color="green"
                style={{ position: "absolute", top: 0, left: 0, zIndex: 2 }}
              >
                OFFER
              </Tag>
            )}

            <Col span={8}>
              <Image
                alt="product_image"
                src={data.image}
                width={100}
                height={100}
                style={{ objectFit: "cover" }}
              />
            </Col>
            <Col span={13}>
              <div style={{ maxHeight: 150 }}>
                <p style={{ display: "block", margin: "2px 0px" }}>
                  <Text
                    ellipsis
                    style={{
                      width: "70vw",
                      color: "#3F9B42",
                      fontWeight: "bold",
                    }}
                  >
                    {data.name}
                  </Text>
                </p>

                {DB_Services.verifyOnOffer(data.start_date, data.end_date) && (
                  <span style={{ display: "flex" }}>
                    <p
                      style={{
                        display: "block",
                        margin: "8px 0px",
                        color: "#707070",
                      }}
                    >
                      Ksh. {data.offer_price}
                    </p>
                    <h3
                      style={{
                        textDecoration: "line-through",
                        lineHeight: 3,
                        marginLeft: 24,
                        fontSize: "0.7rem",
                      }}
                    >
                      Ksh {data.price}
                    </h3>
                  </span>
                )}

                {!DB_Services.verifyOnOffer(data.start_date, data.end_date) && (
                  <p
                    style={{
                      display: "block",
                      margin: "8px 0px",
                      color: "#707070",
                    }}
                  >
                    Ksh. {data.price}
                  </p>
                )}

                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    width: "100%",
                  }}
                >
                  <Text color="#E5E055" style={{ marginRight: 12 }}>
                    {data.quantity} Items
                  </Text>
                  <Tag color="green">
                    <Text ellipsis style={{ width: 50, margin: 0, padding: 0 }}>
                      {data.category.toString().toUpperCase()}
                    </Text>
                  </Tag>
                </div>
              </div>
            </Col>
            <Col span={1}>
              <Dropdown overlay={menu} trigger={["click"]}>
                <Button type="link" color="#707070">
                  <MoreOutlined />
                </Button>
              </Dropdown>
            </Col>
          </Row>

          <Modal
            title="Edit product"
            onOk={handleOkEdit}
            visible={editModal}
            onCancel={() => setEditModal(false)}
            okText={loadingEdit ? "Updating" : "Update"}
            confirmLoading={loadingEdit}
          >
            <Form>
              <Form.Item label="Product name">
                <Input
                  value={editedName}
                  onChange={(e) => setEditedName(e.target.value)}
                  size="large"
                />
              </Form.Item>
              <Form.Item label="Price">
                <InputNumber
                  addonBefore="KSH"
                  value={editedPrice}
                  style={{ width: "100%" }}
                  size="large"
                  onChange={(val) => setEditedPrice(val)}
                />
              </Form.Item>
              <Form.Item label="Category">
                <Select
                  style={{ width: "100%" }}
                  size="large"
                  defaultValue={editedCategory}
                  onChange={(category) => setEditedCategory(category)}
                >
                  {[
                    "sodas",
                    "juices",
                    "medicine",
                    "snacks",
                    "legumes & flour",
                    "breads & cakes",
                    "spices & seasoning",
                    "plastics",
                    "milk products",
                    "meat products",
                    "frozen food",
                    "electricals & electronics",
                    "clothing",
                    "dental products",
                    "gas & gas products",
                    "stationary",
                    "fruits & vegetables",
                    "oil & skin products",
                    "hardware products",
                    "other",
                  ].map((category) => (
                    <Option key={category}>{category}</Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item label="Quantity">
                <InputNumber
                  value={editedQuantity}
                  style={{ width: "100%" }}
                  size="large"
                  onChange={(val) => setEditedQuantity(val)}
                />
              </Form.Item>
            </Form>
          </Modal>

          <Modal
            visible={offersModal}
            title="Add To Offers"
            onOk={handleOkOffers}
            onCancel={handleCancel}
            confirmLoading={loading}
            okText={loading ? "Adding" : "Save"}
          >
            <Form>
              <Form.Item label="Offer price" name="offer_price">
                <InputNumber
                  addonBefore="KSH"
                  placeholder="Offer price"
                  value={offer_price}
                  onChange={(val) => setOfferPrice(val)}
                />
              </Form.Item>
              <Form.Item label="Offer validity" name="offer_validity">
                <RangePicker
                  showTime
                  style={{ width: "100%" }}
                  onCalendarChange={(val, dateString) => {
                    setStartDate(dateString[0]);
                    setEndDate(dateString[1]);
                  }}
                />
              </Form.Item>
            </Form>
          </Modal>
        </section>
      );
    };

    const menuFilterProducts = (
      <Menu style={{ maxHeight: 250, overflow: "auto" }}>
        <Menu.Item key={99}>
          <a onClick={() => setFilter("")}>ALL PRODUCTS</a>
        </Menu.Item>
        <Menu.Divider />
        {[
          "sodas",
          "juices",
          "medicine",
          "snacks",
          "legumes & flour",
          "breads & cakes",
          "spices & seasoning",
          "plastics",
          "milk products",
          "meat products",
          "frozen food",
          "electricals & electronics",
          "clothing",
          "dental products",
          "gas & gas products",
          "stationary",
          "fruits & vegetables",
          "oil & skin products",
          "hardware products",
          "other",
        ].map((category, index) => {
          return (
            <>
              <Menu.Item key={index}>
                <a onClick={() => setFilter(category)}>
                  {category.toLocaleUpperCase()}
                </a>
              </Menu.Item>
              <Menu.Divider />
            </>
          );
        })}
      </Menu>
    );

    return (
      <div style={{ position: "relative" }}>
        <Row style={{ width: "100%", marginBottom: 6 }}>
          <Col span={16}>
            <Search
              allowClear
              placeholder="Search product..."
              onSearch={handleSearch}
            />
          </Col>
          {loadingProduct && (
            <Col span={1} style={{ margin: "0px 12px" }}>
              {" "}
              <Spin size="small" />
            </Col>
          )}
          <Col span={2}>
            <Dropdown overlay={menuFilterProducts} trigger={["click"]}>
              <Button type="link" style={{ color: "#707070" }}>
                <FilterOutlined />
              </Button>
            </Dropdown>
          </Col>
        </Row>

        {filter != "" && (
          <div style={{ padding: 2, margin: 0, marginBottom: 6 }}>
            {filter.toUpperCase()}:{" "}
            <Tag color="green">
              {items
                ? items.docs.filter((doc) => doc.data().category == filter)
                    .length
                : "..."}
            </Tag>
          </div>
        )}

        {filter == "" && (
          <div style={{ padding: 2, margin: 0, marginBottom: 16 }}>
            ALL PRODUCTS:{" "}
            <Tag color="green">{items ? items.docs.length : "..."}</Tag>
          </div>
        )}

        <div
          id="content"
          ref={scrollDiv}
          onScroll={fetchMore}
          style={{
            maxHeight: "calc(100vh - 235px)",
            zIndex: -9,
            overflow: "scroll",
          }}
        >
          {suggestions.length > 0 && (
            <>
              <h3>Search results({suggestions.length} products)</h3>
              <div style={{ display: "flex", flexWrap: "wrap" }}>
                {suggestions.map((product) => (
                  <AdminProduct
                    key={product.id}
                    data={product}
                    id={product.id}
                  />
                ))}
              </div>

              <Divider orientation="right">Other products</Divider>
            </>
          )}

          {loadingSearch && (
            <Spin
              style={{
                position: "absolute",
                left: "50%",
                transform: "translateY(-50%)",
              }}
            />
          )}

          <div style={{ display: "flex", flexWrap: "wrap" }}>
            {prods.length > 0 &&
              prods.map((product) => (
                <AdminProduct
                  key={product.id}
                  data={product.data}
                  id={product.id}
                />
              ))}
          </div>

          <Button
            onClick={() => setAddModal(true)}
            style={{
              position: "fixed",
              bottom: "3rem",
              right: "0.5rem",
              width: 48,
              height: 48,
              borderRadius: "50%",
              background: "#3F9B42",
              zIndex: 99,
            }}
          >
            <PlusOutlined style={{ color: "white" }} />
          </Button>

          <Modal
            title="Add product"
            visible={addModal}
            okText={uploadLoading ? "Uploading" : "Save"}
            onOk={handleOkAdd}
            onCancel={() => setAddModal(false)}
            confirmLoading={uploadLoading}
          >
            <Form>
              <Form.Item label="Product name">
                <Input
                  value={productName}
                  size="large"
                  onChange={(e) => setProductName(e.target.value)}
                  placeholder="Name"
                />
              </Form.Item>
              <Form.Item label="Price">
                <InputNumber
                  size="large"
                  style={{ width: "100%" }}
                  onChange={(val) => setProductPrice(val)}
                  addonBefore="KSH"
                  placeholder="Price"
                  value={productPrice}
                />
              </Form.Item>
              <Form.Item label="Quantity">
                <InputNumber
                  size="large"
                  style={{ width: "100%" }}
                  onChange={(val) => setProductQuantity(val)}
                  placeholder="Quantity"
                  value={productQuantity}
                />
              </Form.Item>
              <Form.Item label="Category">
                <Select
                  style={{ width: "100%" }}
                  size="large"
                  defaultValue="sodas"
                  onChange={(category) => setCategory(category)}
                >
                  {[
                    "sodas",
                    "juices",
                    "medicine",
                    "snacks",
                    "legumes & flour",
                    "breads & cakes",
                    "spices & seasoning",
                    "plastics",
                    "milk products",
                    "meat products",
                    "frozen food",
                    "electricals & electronics",
                    "clothing",
                    "dental products",
                    "gas & gas products",
                    "stationary",
                    "fruits & vegetables",
                    "oil & skin products",
                    "hardware products",
                    "other",
                  ].map((category) => (
                    <Option key={category}>{category.toUpperCase()}</Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item label="Product Image">
                {image && imageUrl ? (
                  <img
                    src={imageUrl}
                    style={{ width: "100%", objectFit: "cover" }}
                  />
                ) : (
                  <input
                    type="file"
                    accept="image/*"
                    onChange={onImageChange}
                  />
                )}
              </Form.Item>
            </Form>
          </Modal>
          {loadingMore && (
            <Spin
              style={{
                position: "absolute",
                bottom: "0rem",
                left: "50%",
                transform: "translateY(-50%)",
              }}
            />
          )}
        </div>
      </div>
    );
  };

  const OrdersComponent = () => {
    const [keyword, setKeyword] = useState("");
    const [filter, setFilter] = useState("awaiting packing");
    const [mobile, setMobile] = useState("");

    const [users, usersLoading, usersError] = useCollection(
      collection(db, "users"),
      {}
    );

    const menuFilterOrders = (
      <Menu>
        <Menu.Item key="5">
          <a onClick={() => setFilter("awaiting packing")}>Awaiting packing</a>
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="3">
          <a onClick={() => setFilter("packed")}>Packed</a>
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="4">
          <a onClick={() => setFilter("delivered")}>Delivered</a>
        </Menu.Item>
      </Menu>
    );

    const AdminOrder = ({ isVisible, data, id, filter }) => {
      const [confirmPacked, setConfirmPacked] = useState(false);
      const [popVisible, setPopVisible] = useState(false);
      const [confirmDelivered, setConfirmDelivered] = useState(false);
      const [popVisibleB, setPopVisibleB] = useState(false);

      const onChangePacked = (user) => {
        setConfirmPacked(true);
        DB_Services.markPacked({ user })
          .then((val) => console.log(val))
          .then(() => setConfirmPacked(false));
      };

      const onChangeDelivered = (user) => {
        setConfirmDelivered(true);
        DB_Services.markDelivered({ user })
          .then((val) => console.log(val))
          .then(() => setConfirmDelivered(false));
      };

      const getTotal = (arr) => {
        let tot = 0;
        if (arr == 0) {
          return tot;
        } else {
          arr.forEach((cartItem) => {
            tot += cartItem.quantity * cartItem.product.price;
          });
          return tot;
        }
      };

      return (
        <div
          style={{
            width: "100%",
            marginBottom: 12,
            maxWidth: 400,
            margin: "0px 0px 12px 12px",
          }}
        >
          <Row style={{ border: "#f1f1f1 1px solid", padding: 12 }}>
            <Col span={3}>
              <Avatar>{data.displayName.charAt(0)}</Avatar>
            </Col>
            <Col span={18} offset={1}>
              <Text ellipsis style={{ display: "block", color: "#3F9B42" }}>
                {data.displayName}
              </Text>
              <p style={{ fontSize: "0.8rem" }}>{data.phoneNumber}</p>
              <p style={{ fontSize: "0.8rem", color: "#707070" }}>
                {data.location}
              </p>
            </Col>
          </Row>
          <Container
            style={{
              display: isVisible ? "block" : "none",
              background: "#f1f1f1",
            }}
          >
            <Divider orientation="right">Products</Divider>
            {filter == "awaiting packing" && data.cart && (
              <>
                {data.cart
                  .filter(
                    (cartItem) =>
                      cartItem.delivered == false &&
                      cartItem.packed == false &&
                      (cartItem.paid == true || cartItem.deliver_first == true)
                  )
                  .map((cartItem, index) => {
                    return (
                      <div
                        style={{
                          borderBottom: "#f1f1f1 1px dashed",
                          display: "flex",
                          justifyContent: "space-between",
                          padding: "8px 0px",
                          width: "90%",
                        }}
                        key={index}
                      >
                        <Text ellipsis style={{ width: 120 }}>
                          {cartItem.product.name}
                        </Text>
                        <Tag color="green" style={{ height: 24, width: 24 }}>
                          {cartItem.quantity}
                        </Tag>
                        <span>@</span>
                        <span>{cartItem.product.price}</span>
                        <span>
                          {cartItem.quantity * cartItem.product.price}
                        </span>
                      </div>
                    );
                  })}
                <p style={{ width: "100%", textAlign: "right", padding: 12 }}>
                  Tot :{" "}
                  <strong>
                    Ksh.{" "}
                    {getTotal(
                      data.cart
                        ? data.cart.filter(
                            (cartItem) =>
                              cartItem.paid == false &&
                              cartItem.packed == false &&
                              cartItem.delivered == false &&
                              cartItem.deliver_first == true
                          )
                        : 0
                    )}
                  </strong>
                </p>
                <br />

                <Divider />
                <Popconfirm
                  visible={popVisible}
                  title="Set this order as packed ? This action is irreversible"
                  okButtonProps={{ loading: confirmPacked }}
                  onConfirm={() => onChangePacked(id)}
                  onCancel={() => setPopVisible(false)}
                  okText="Yes, sure"
                >
                  <Button
                    onClick={() => setPopVisible(true)}
                    type="primary"
                    block
                  >
                    Mark as packed
                  </Button>
                </Popconfirm>
              </>
            )}

            {filter == "packed" && data.cart && (
              <>
                {
                  <>
                    <Divider orientation="left">Paid</Divider>
                    {data.cart
                      .filter(
                        (cartItem) =>
                          cartItem.packed == true &&
                          cartItem.delivered == false &&
                          cartItem.deliver_first == false
                      )
                      .map((cartItem, index) => (
                        <div
                          style={{
                            borderBottom: "#f1f1f1 1px dashed",
                            display: "flex",
                            justifyContent: "space-between",
                            padding: "8px 0px",
                            width: "90%",
                          }}
                          key={index}
                        >
                          <Text ellipsis style={{ width: 120 }}>
                            {cartItem.product.name}
                          </Text>
                          <Tag color="green" style={{ height: 24, width: 24 }}>
                            {cartItem.quantity}
                          </Tag>
                          <span>@</span>
                          <span>{cartItem.product.price}</span>
                          <span>
                            {cartItem.quantity * cartItem.product.price}
                          </span>
                        </div>
                      ))}
                    <p
                      style={{ width: "100%", textAlign: "right", padding: 12 }}
                    >
                      Tot :{" "}
                      <strong>
                        Ksh.{" "}
                        {getTotal(
                          data.cart
                            ? data.cart.filter(
                                (cartItem) =>
                                  cartItem.packed == true &&
                                  cartItem.delivered == false &&
                                  cartItem.deliver_first == false
                              )
                            : 0
                        )}
                      </strong>
                    </p>
                  </>
                }
                {
                  <>
                    <Divider orientation="left">Pay on delivery</Divider>
                    {data.cart
                      .filter(
                        (cartItem) =>
                          cartItem.packed == true &&
                          cartItem.delivered == false &&
                          cartItem.deliver_first == true
                      )
                      .map((cartItem, index) => (
                        <div
                          style={{
                            borderBottom: "#f1f1f1 1px dashed",
                            display: "flex",
                            justifyContent: "space-between",
                            padding: "8px 0px",
                            width: "90%",
                          }}
                          key={index}
                        >
                          <Text ellipsis style={{ width: 120 }}>
                            {cartItem.product.name}
                          </Text>
                          <Tag color="green" style={{ height: 24, width: 24 }}>
                            {cartItem.quantity}
                          </Tag>
                          <span>@</span>
                          <span>{cartItem.product.price}</span>
                          <span>
                            {cartItem.quantity * cartItem.product.price}
                          </span>
                        </div>
                      ))}
                    <p
                      style={{ width: "100%", textAlign: "right", padding: 12 }}
                    >
                      Tot :{" "}
                      <strong>
                        Ksh.{" "}
                        {getTotal(
                          data.cart
                            ? data.cart.filter(
                                (cartItem) =>
                                  cartItem.packed == true &&
                                  cartItem.delivered == false &&
                                  cartItem.deliver_first == true
                              )
                            : 0
                        )}
                      </strong>
                    </p>

                    <Popconfirm
                      visible={popVisibleB}
                      title="Set this order as delivered ? This action is irreversible"
                      okButtonProps={{ loading: confirmDelivered }}
                      onConfirm={() => onChangeDelivered(id)}
                      onCancel={() => setPopVisibleB(false)}
                      okText="Yes, sure"
                    >
                      <Button
                        onClick={() => setPopVisibleB(true)}
                        type="primary"
                        block
                      >
                        Mark as delivered
                      </Button>
                    </Popconfirm>
                  </>
                }
              </>
            )}
            {filter == "delivered" && data.cart && (
              <>
                {data.cart
                  .filter(
                    (cartItem) =>
                      cartItem.paid == true &&
                      cartItem.packed == true &&
                      cartItem.delivered == true
                  )
                  .map((cartItem) => {
                    return (
                      <Row
                        style={{
                          padding: "12px 0px",
                          borderBottom: "#f1f1f1 1px dashed",
                        }}
                        key={cartItem.product.id}
                      >
                        <Col span={6}>
                          <Tooltip title={cartItem.product.name}>
                            <Text ellipsis style={{ width: "25vw" }}>
                              {cartItem.product.name}
                            </Text>
                          </Tooltip>
                        </Col>
                        <Col span={2} offset={1}>
                          {cartItem.quantity}
                        </Col>
                        <Col span={1}>@</Col>
                        <Col span={3}>{cartItem.product.price}</Col>
                        <Col span={4}>
                          <strong>
                            {cartItem.product.price * cartItem.quantity}Ksh
                          </strong>
                        </Col>
                        <Col span={7}>
                          <p style={{ fontSize: "0.8rem" }}>
                            {new Date(cartItem.added).toDateString()}
                          </p>
                        </Col>
                      </Row>
                    );
                  })}
              </>
            )}
          </Container>
          <Divider />
        </div>
      );
    };

    const onSearch = (val) => {
      setMobile(val);
    };

    return (
      <>
        <Row style={{ width: "100%" }}>
          <Col span={12}>
            <p style={{ padding: "12px 4px", margin: 0 }}>
              Showing : <Tag color={"green"}>{filter.toUpperCase()}</Tag>
            </p>
          </Col>
          <Col span={10}>
            <Search
              allowClear
              onSearch={onSearch}
              placeholder="Search with mobile no."
            />
          </Col>
          <Col span={1}>
            <Dropdown overlay={menuFilterOrders} trigger={["click"]}>
              <Button type="link" style={{ color: "#3F9B42" }}>
                <FilterOutlined />
              </Button>
            </Dropdown>
          </Col>
        </Row>

        <br />
        <div
          style={{
            maxHeight: "calc(100vh - 270px)",
            overflow: "scroll",
            width: "100%",
          }}
        >
          {!users && usersLoading && <p>Loading...</p>}
          {users && users.docs.length < 1 && <p>No users yet</p>}
          {usersError && <p>Error..</p>}

          {/* to deliver , unpacked , undelivered */}
          <div style={{ display: "flex", flexWrap: "wrap" }}>
            {users &&
              users.docs.length > 0 &&
              users.docs
                .filter((user) =>
                  mobile !== "" ? user.data().phoneNumber == mobile : user
                )
                .filter((user) => {
                  if (filter == "awaiting packing") {
                    return user
                      .data()
                      .cart.some(
                        (cartItem) =>
                          cartItem.delivered == false &&
                          cartItem.packed == false &&
                          (cartItem.deliver_first == true ||
                            cartItem.paid == true)
                      );
                  } else if (filter == "paid") {
                    return user
                      .data()
                      .cart.some(
                        (cartItem) =>
                          cartItem.paid == true &&
                          cartItem.packed == false &&
                          cartItem.delivered == false
                      );
                  } else if (filter == "packed") {
                    return user
                      .data()
                      .cart.some(
                        (cartItem) =>
                          cartItem.packed == true && cartItem.delivered == false
                      );
                  } else if (filter == "delivered") {
                    return user
                      .data()
                      .cart.some(
                        (cartItem) =>
                          cartItem.paid == true &&
                          cartItem.packed == true &&
                          cartItem.delivered == true
                      );
                  }
                })
                .map((user, index) => {
                  let data = user.data();
                  return (
                    <AdminOrder
                      key={index}
                      isVisible={true}
                      data={data}
                      id={user.id}
                      filter={filter}
                    />
                  );
                })}
          </div>

          {users &&
            users.docs.length > 0 &&
            users.docs.filter((user) => {
              if (filter == "awaiting packing") {
                return user
                  .data()
                  .cart.some(
                    (cartItem) =>
                      cartItem.paid == false &&
                      cartItem.packed == false &&
                      cartItem.delivered == false &&
                      cartItem.deliver_first == true
                  );
              } else if (filter == "paid") {
                return user
                  .data()
                  .cart.some(
                    (cartItem) =>
                      cartItem.paid == true &&
                      cartItem.packed == false &&
                      cartItem.delivered == false
                  );
              } else if (filter == "packed") {
                return user
                  .data()
                  .cart.some(
                    (cartItem) =>
                      cartItem.packed == true && cartItem.delivered == false
                  );
              } else if (filter == "delivered") {
                return user
                  .data()
                  .cart.some(
                    (cartItem) =>
                      cartItem.paid == true &&
                      cartItem.packed == true &&
                      cartItem.delivered == true
                  );
              }
            }).length < 1 && <Empty description="No orders yet" />}
        </div>
      </>
    );
  };

  const StatsComponent = () => {
    const [items, itemsLoading, itemsError] = useCollection(
      query(collection(db, "products"), orderBy("added", "desc"))
    );

    const [customers, customersLoading, customersError] = useCollection(
      query(collection(db, "users"))
    );

    const getSalesCount = () => {
      let sales = 0;

      customers.docs.forEach((customer) => {
        customer.data().cart.forEach((cartItem) => {
          if (cartItem.paid == true) {
            sales = sales + cartItem.product.price;
          }
        });
      });

      return sales;
    };

    return (
      <Row gutter={16} style={{ width: "100%" }}>
        <Col xs={24} md={8} lg={6} style={{ marginBottom: 12 }}>
          <Card>
            <Statistic
              title="Products"
              value={items ? items.docs.length : "..."}
              prefix={<CodeSandboxOutlined />}
            />
          </Card>
        </Col>
        <Col xs={24} md={8} lg={6} style={{ marginBottom: 12 }}>
          <Card>
            <Statistic
              title="Customers"
              value={customers ? customers.docs.length : "..."}
              prefix={<UserOutlined />}
            />
          </Card>
        </Col>

        <Col xs={24} md={8} lg={6} style={{ marginBottom: 12 }}>
          <Card>
            <Statistic
              title="All time sales (KES)"
              value={customers ? getSalesCount() : "..."}
              prefix={<DollarOutlined />}
            />
          </Card>
        </Col>
      </Row>
    );
  };

  return (
    <>
      <Container
        style={{
          position: "absolute",
          top: 16,
          width: "95%",
          minHeight: "95vh",
        }}
      >
        <Title
          level={3}
          style={{ color: "#3f9b42", letterSpacing: "-0.05rem" }}
        >
          Dashboard
        </Title>
        <Tabs defaultActiveKey="1">
          {/* <TabPane tab="Dashboard" key="1"></TabPane> */}
          <TabPane tab="Products" key="2">
            <ProductsComponent />
          </TabPane>
          <TabPane tab="Orders" key="3">
            <OrdersComponent />
          </TabPane>
          <TabPane tab="Stats" key="4">
            <StatsComponent />
          </TabPane>
        </Tabs>
      </Container>
    </>
  );
};
